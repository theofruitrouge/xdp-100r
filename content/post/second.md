+++
date = "2015-12-2T14:10:00+03:00"
draft = false
title = "Ressources"
weight = 2
+++

Now that you setup your working environnement, here is a list of ressources (mostly found on [**this thread**](https://www.head-fi.org/threads/onkyo-dp-x1-dual-sabre-dacs-balanced-sabre-btl-amp-mqa-dsd-256-android-5.780642/)).

#### Thank you to all users which developped TWRP, catched OTA updates and provided help. Feel free to contribute.

* TWRP 3.0.2 **by efenex** - [**Github**](https://github.com/efenex/TWRP_Rai_Zin_32) - [**direct download**](https://github.com/efenex/TWRP_Rai_Zin_32/raw/master/twrp-3.0.2-1-Rai_Zin_32.img) [[#mirror](http://www.filedropper.com/twrp-302-1-raizin32)]
* OTA update 1.26 - **MAKE A BACKUP FIRST** - [Link from **pegasus21**](https://android.googleapis.com/packages/ota/onkyo_raizin32_raizin32/914e21f7c8433e4fc1819cc005d4522ca25e82f3.signed-Onkyo_32G-ota_update.zip) [[#mirror ](#)]
* OTA update 1.28 - **MAKE A BACKUP FIRST** - [Link from **pegasus21**](https://android.googleapis.com/packages/ota/onkyo_raizin32_raizin32/df30d6331a9d059b201363a70c664bed92470bf2.signed-Onkyo_32G-ota_update.zip) [[#mirror ](#)]
* TWRP BACKUP **by damex** - Download [boot+system](https://drive.google.com/file/d/0B9WN-DrZ4uGTclZvNG9BSVNpQUk/view?usp=sharing) [[#mirror](#)] and (optional) [stock recovery](https://drive.google.com/file/d/0B9WN-DrZ4uGTVWlNZlpxcXRueUU/view?usp=sharing) [[#mirror](http://www.filedropper.com/recovery_12)]
 
## MD5 sums
``
217c8857899f07abd89ce05bda58d849
``
*twrp-3.0.2-1-Rai_Zin_32.img

``
4cb86ff52f9486ab9198e25f8e401473
``
*914e21f7c8433e4fc1819cc005d4522ca25e82f3.signed-Onkyo_32G-ota_update.zip

``
663443fbe849d8bd33c5951ea526a17d
``
*df30d6331a9d059b201363a70c664bed92470bf2.signed-Onkyo_32G-ota_update.zip

``
4f32cbb8289ae4a9a7dd09c9f2d4ab34
``
*boot+system.zip

``
d9b9d402d6dedd382561291923281243
``
*recovery.zip

