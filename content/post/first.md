+++
date = "2015-12-2T14:10:00+03:00"
draft = false
title = "Welcome !"
weight = 1
+++

This website is a guide and ressource list to hack the Pioneer XDP-100R (also known as the ONKYO DP-X1), which is a rare device.

>> ## **YOU** are invited to contribute on the [Gitlab repository](https://gitlab.com/theofruitrouge/xdp-100r).

To start you need **adb and fastboot** binaries. Download system wide installer [here for Windows](https://forum.xda-developers.com/showthread.php?t=2588979). For Linux and Mac, you can use [the platform tool package](https://developer.android.com/studio/releases/platform-tools)

Once it's done, you can activate android debugging under Developers Options under Settings (see [here](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/) how to activate developpers settings)

Then **plug your device via USB** and type :
```
adb devices
```
You should see your device in the list. You might need to authorize you computer (a popup appears on the device automatically, if not disable an re-enable USB debugging) and tick the remember option if you always plan to use that computer.
