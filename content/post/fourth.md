+++
date = "2015-12-2T14:10:00+03:00"
draft = false
title = "Sources"
weight = 4
+++

* TWRP BACKUP : [https://www.head-fi.org/threads/onkyo-dp-x1-dual-sabre-dacs-balanced-sabre-btl-amp-mqa-dsd-256-android-5.780642/page-749#post_12846468](https://www.head-fi.org/threads/onkyo-dp-x1-dual-sabre-dacs-balanced-sabre-btl-amp-mqa-dsd-256-android-5.780642/page-749#post_12846468)
* OTA updates : [https://www.head-fi.org/threads/onkyo-dp-x1-dual-sabre-dacs-balanced-sabre-btl-amp-mqa-dsd-256-android-5.780642/page-234#post_12254667](https://www.head-fi.org/threads/onkyo-dp-x1-dual-sabre-dacs-balanced-sabre-btl-amp-mqa-dsd-256-android-5.780642/page-234#post_12254667)
* Viper4Anroid : [https://www.head-fi.org/threads/onkyo-dp-x1-dual-sabre-dacs-balanced-sabre-btl-amp-mqa-dsd-256-android-5.780642/page-662#post-12696636](https://www.head-fi.org/threads/onkyo-dp-x1-dual-sabre-dacs-balanced-sabre-btl-amp-mqa-dsd-256-android-5.780642/page-662#post-12696636)
* TWRP Original : [https://www.head-fi.org/threads/onkyo-dp-x1-dual-sabre-dacs-balanced-sabre-btl-amp-mqa-dsd-256-android-5.780642/page-621#post-12657959](https://www.head-fi.org/threads/onkyo-dp-x1-dual-sabre-dacs-balanced-sabre-btl-amp-mqa-dsd-256-android-5.780642/page-621#post-12657959)