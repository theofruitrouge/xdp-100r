+++
date = "2015-12-2T14:10:00+03:00"
draft = false
title = "Tutorial part"
weight = 3
+++

Here I will show you my personnal way of doing stuff on my XDP-100R.

## Boot into TWRP without flashing

First, I did not flash TWRP but I just boot into it when I need to make a backup or flash a zip (yes it does require a computer).

If you are still on the "adb devices" step, you need to execute the folowing commands :

```
adb reboot bootloader
```
Your device will reboot and show a black screen.

```
fastboot boot twrp-3.0.2-1-Rai_Zin_32.img
```
This will load TWRP in temporary memory.

``
Note: the TWRP image should be in the same forder than your terminal. Otherwise replace "twrp-3.0.2-1-Rai_Zin_32.img" by the full path (e.g. C:\Users\TestUser\Downloads\twrp-3.0.2-1-Rai_Zin_32.img **OR** /Users/testuser/Download/twrp-3.0.2-1-Rai_Zin_32.img)
``

```
fastboot reboot
```
Your device should reboot into TWRP. On the next reboot it will boot normally.

I use TWRP to acess internal storage via MTP (e.g. transfer zip files and install them, make a full backup and save it to your computer, etc.)

## Root your device (systemlessly + open source !)

You should know that I come from the FLOSS (Free Libre Open Source Software) community and thus I prefer to use open source methods. I don't like to use "kingroot" or "supersu" as these methods relies on proprietary software.

Thus I recommand [Magisk](https://www.xda-developers.com/how-to-install-magisk/), an open source root method (and much more). It doesn't modify the system partition, which from my point of view is very usefull and smart.

To work, Magisk patches the boot partition. The best way to install it is flashing a zip in TWRP (available on [the official XDA thread](https://forum.xda-developers.com/apps/magisk/official-magisk-v7-universal-systemless-t3473445)).

It works flawlessly in my XDP-100R and I enjoy open source root on all my devices !

## Make you battery last for days

So the title is very interesting, but how to do that ?

First it's not a magical solution but what helps a lot is to disable (or remove !) all the Google Proprietary Binaries from the original software. Without root you can simply disable all packages with "google" in their name, and if you have root you can totally uninstall these packages.

If you need GSF (Google Service Framework) for some reason, you can always install an open source implementation called [microG](https://microg.org/).